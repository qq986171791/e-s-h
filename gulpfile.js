var gulp = require("gulp");
var browserSync = require("browser-sync").create();
var sass = require("gulp-sass");
var reload = browserSync.reload;
var connect = require("gulp-connect");

// 静态服务器 + 监听 scss/html 文件
gulp.task("serve", ["sass"], function() {
  connect.server({
    livereload: true
  });

  gulp.watch("./main.scss", ["sass"]);
  gulp.watch("./index.html").on("change", connect.reload);
});

// scss编译后的css将注入到浏览器里实现更新
gulp.task("sass", function() {
  return gulp
    .src("./main.scss")
    .pipe(sass())
    .pipe(gulp.dest("./"))
    .pipe(connect.reload());
});

gulp.task("default", ["serve"]);
